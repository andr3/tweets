let data = {
	username: "andr3", // No leading @ here
	homeLabel: "andr3.net",
	homeUrl: "https://andr3.net/",
};

data.avatar = `http://imgs.andr3.net/avatar.png`;

module.exports = data;
